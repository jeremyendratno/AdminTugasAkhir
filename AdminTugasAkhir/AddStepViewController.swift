//
//  AddStepViewController.swift
//  AdminTugasAkhir
//
//  Created by Jeremy on 08/06/21.
//

import UIKit

class AddStepViewController: UIViewController, UIImagePickerControllerDelegate, UITextFieldDelegate, UITextViewDelegate, UINavigationControllerDelegate {
    @IBOutlet weak var namaTextField: UITextField!
    @IBOutlet weak var deskripsiTextField: UITextView!
    @IBOutlet weak var pictureImageView: UIImageView!
    
    var imagePicker = UIImagePickerController()
    var stepDelegate: StepViewController?
    
    var uploadedImage = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        namaTextField.delegate = self
        deskripsiTextField.delegate = self
    }
    
    @IBAction func uploadButton(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @IBAction func Add(_ sender: Any) {
        stepDelegate?.addStep(nama: namaTextField.text ?? "", deskripsi: deskripsiTextField.text ?? "", gambar: uploadedImage)
        self.dismiss(animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            uploadedImage = image
            pictureImageView.image = uploadedImage
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
}
