//
//  ViewController.swift
//  AdminTugasAkhir
//
//  Created by Jeremy on 08/06/21.
//

import UIKit

class ViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate, UITextViewDelegate {
    @IBOutlet weak var namaTextField: UITextField!
    @IBOutlet weak var deskripsiTextView: UITextView!
    @IBOutlet weak var pictureImageView: UIImageView!
    
    var imagePicker = UIImagePickerController()
    var group = DispatchGroup()
    
    var nama = ""
    var deskripsi = ""
    var uploadedImage: UIImage = UIImage()
    var newInsertedId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        namaTextField.delegate = self
        deskripsiTextView.delegate = self
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextView))
    }
    
    @IBAction func uploadButton(_ sender: Any) {
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
            imagePicker.sourceType = .photoLibrary
            imagePicker.allowsEditing = false
            present(imagePicker, animated: true, completion: nil)
        }
    }
    
    @objc func nextView() {
        nama = namaTextField.text ?? ""
        deskripsi = deskripsiTextView.text ?? ""
        upload(imageToUpload: uploadedImage, imgKey: nama)
        uploadData(nama: nama, gambar: nama, deskripsi: deskripsi)
        
        group.notify(queue: .main) {
            self.performSegue(withIdentifier: "Bahan", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let data = segue.destination as! BahanViewController
        data.id = newInsertedId
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)
        if let image = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            uploadedImage = image
            pictureImageView.image = uploadedImage
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textView(_ textView: UITextView, shouldChangeTextIn range: NSRange, replacementText text: String) -> Bool {
        if text == "\n" {
            textView.resignFirstResponder()
            return false
        }
        return true
    }
    
    func upload(imageToUpload: UIImage, imgKey: String) {
        group.enter()
        let myUrl = NSURL(string: "http://128.199.242.164/tugasakhir/add_image.php");
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";

        let param = [
            "purpose"    : "uploadImage"
        ]

        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
        let imageData = imageToUpload.jpegData(compressionQuality: 1)
        if imageData == nil  {
            return
        }

        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "file", imageDataKey: imageData! as NSData, boundary: boundary, imgKey: imgKey) as Data

        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
        
            if error != nil {
                print("error=\(error!)")
                return
            }

            self.group.leave()
        }
        
        task.resume()
    }




    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, imgKey: String) -> NSData {
        let body = NSMutableData();
            
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
            
        let filename = "\(imgKey).jpg"
        let mimetype = "image/jpg"

        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")

        return body
    }

    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
    
    func uploadData( nama: String, gambar: String, deskripsi: String ) {
        group.enter()
        let url = NSURL(string: "http://128.199.242.164/tugasakhir/add_resep.php")
                
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "POST"
                
        var dataString = "&nama=\(nama)"
            dataString = dataString + "&gambar=\(gambar)"
            dataString = dataString + "&deskripsi=\(deskripsi)"
            
        let dataD = dataString.data(using: .utf8)
        
        let uploadJob = URLSession.shared.uploadTask(with: request, from: dataD) { data, response, error in
            
            let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue) ?? "0"
            self.newInsertedId = responseString as String
            
            self.group.leave()
        }
        
        uploadJob.resume()
    }

}

extension NSMutableData {
    func appendString(string: String) {
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
}


