//
//  BahanViewController.swift
//  AdminTugasAkhir
//
//  Created by Jeremy on 08/06/21.
//

import UIKit

class BahanViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var group = DispatchGroup()
    
    var bahans: [(String, String)] = []
    var id: String = String()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.delegate = self
        tableView.dataSource = self
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(add))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(nextView))
    }
    
    func addBahan(bahan: String, ukuran: String) {
        bahans.append((bahan, ukuran))
        tableView.reloadData()
    }
    
    @objc func add() {
        performSegue(withIdentifier: "Add Bahan", sender: self)
    }
    
    @objc func nextView() {
        for bahan in bahans {
            uploadData(nama: bahan.0, ukuran: bahan.1, resepId: id)
        }
        
        group.notify(queue: .main) {
            self.performSegue(withIdentifier: "Step", sender: self)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Add Bahan" {
            let data = segue.destination as! AddBahanViewController
            data.bahanDelegate = self
        }
        
        if segue.identifier == "Step" {
            let data = segue.destination as! StepViewController
            data.id = id
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        bahans.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Bahan", for: indexPath) as! BahanCell
        cell.nama.text = bahans[indexPath.row].0
        cell.ukuran.text = bahans[indexPath.row].1
        return cell
    }
    
    func uploadData( nama: String, ukuran: String, resepId: String ) {
        group.enter()
        let url = NSURL(string: "http://128.199.242.164/tugasakhir/add_bahan.php")
                
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "POST"
                
        var dataString = "&nama=\(nama)"
            dataString = dataString + "&ukuran=\(ukuran)"
            dataString = dataString + "&resepId=\(resepId)"
            
        let dataD = dataString.data(using: .utf8)
        
        let uploadJob = URLSession.shared.uploadTask(with: request, from: dataD) { data, response, error in
            self.group.leave()
        }
        
        uploadJob.resume()
    }
}

class BahanCell: UITableViewCell {
    @IBOutlet weak var nama: UILabel!
    @IBOutlet weak var ukuran: UILabel!
}
