//
//  AddBahanViewController.swift
//  AdminTugasAkhir
//
//  Created by Jeremy on 08/06/21.
//

import UIKit

class AddBahanViewController: UIViewController {
    @IBOutlet weak var bahanTextField: UITextField!
    @IBOutlet weak var ukuranTextField: UITextField!
    
    var bahanDelegate: BahanViewController?

    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    @IBAction func addButton(_ sender: Any) {
        bahanDelegate?.addBahan(bahan: bahanTextField.text ?? "", ukuran: ukuranTextField.text ?? "")
        self.dismiss(animated: true)
    }
    
}
