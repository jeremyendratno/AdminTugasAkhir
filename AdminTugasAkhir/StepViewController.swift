//
//  StepViewController.swift
//  AdminTugasAkhir
//
//  Created by Jeremy on 08/06/21.
//

import UIKit

struct Step {
    var nama: String
    var deskripsi: String
    var gambar: UIImage
}

class StepViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!
    
    var group = DispatchGroup()
    
    var steps: [Step] = []
    var id: String = String()
    var indikasi = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(add))
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Done", style: .plain, target: self, action: #selector(done))
    }
    
    @objc func add() {
        performSegue(withIdentifier: "Add Step", sender: self)
    }
    
    @objc func done() {
        for step in steps {
            indikasi += 1
            uploadData(nama: step.nama, diskripsi: step.deskripsi, indikasi: String(indikasi), gambar: "\(step.nama)\(indikasi)", resepId: id)
            upload(imageToUpload: step.gambar, imgKey: "\(step.nama)\(indikasi)")
        }
        
        group.notify(queue: .main) {
            self.performSegue(withIdentifier: "Done", sender: self)
        }
    }
    
    func addStep(nama: String, deskripsi: String, gambar: UIImage) {
        steps.append(Step(nama: nama, deskripsi: deskripsi, gambar: gambar))
        tableView.reloadData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "Add Step" {
            let data = segue.destination as! AddStepViewController
            data.stepDelegate = self
        }
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        steps.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Step", for: indexPath) as! StepCell
        cell.namaLabel.text = steps[indexPath.row].nama
        return cell
    }
    
    func uploadData( nama: String, diskripsi: String, indikasi: String, gambar: String, resepId: String ) {
        group.enter()
        let url = NSURL(string: "http://128.199.242.164/tugasakhir/add_stepResep.php")
                
        var request = URLRequest(url: url! as URL)
        request.httpMethod = "POST"
                
        var dataString = "&nama=\(nama)"
            dataString = dataString + "&diskripsi=\(diskripsi)"
            dataString = dataString + "&indikasi=\(indikasi)"
            dataString = dataString + "&gambar=\(gambar)"
            dataString = dataString + "&resepId=\(resepId)"
        
            
        let dataD = dataString.data(using: .utf8)
        
        let uploadJob = URLSession.shared.uploadTask(with: request, from: dataD) { data, response, error in
            self.group.leave()
        }
        
        uploadJob.resume()
    }
    
    func upload(imageToUpload: UIImage, imgKey: String) {
        group.enter()
        let myUrl = NSURL(string: "http://128.199.242.164/tugasakhir/add_image.php");
        let request = NSMutableURLRequest(url:myUrl! as URL);
        request.httpMethod = "POST";

        let param = [
            "purpose"    : "uploadImage"
        ]

        let boundary = generateBoundaryString()
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
            
        let imageData = imageToUpload.jpegData(compressionQuality: 1)
        if imageData == nil  {
            return
        }

        request.httpBody = createBodyWithParameters(parameters: param, filePathKey: "file", imageDataKey: imageData! as NSData, boundary: boundary, imgKey: imgKey) as Data

        let task = URLSession.shared.dataTask(with: request as URLRequest) { data, response, error in
        
            if error != nil {
                print("error=\(error!)")
                return
            }

            self.group.leave()
        }
        
        task.resume()
    }




    func createBodyWithParameters(parameters: [String: String]?, filePathKey: String?, imageDataKey: NSData, boundary: String, imgKey: String) -> NSData {
        let body = NSMutableData();
            
        if parameters != nil {
            for (key, value) in parameters! {
                body.appendString(string: "--\(boundary)\r\n")
                body.appendString(string: "Content-Disposition: form-data; name=\"\(key)\"\r\n\r\n")
                body.appendString(string: "\(value)\r\n")
            }
        }
            
        let filename = "\(imgKey).jpg"
        let mimetype = "image/jpg"

        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey!)\"; filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
        body.append(imageDataKey as Data)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)--\r\n")

        return body
    }

    func generateBoundaryString() -> String {
        return "Boundary-\(NSUUID().uuidString)"
    }
}

class StepCell: UITableViewCell {
    @IBOutlet weak var namaLabel: UILabel!
}
